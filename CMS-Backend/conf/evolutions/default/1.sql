# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table артифакты (
  ид                            serial not null,
  ид_владельца                  integer,
  constraint pk_артифакты primary key (ид)
);

create table события (
);

create table пол (
  ид                            serial not null,
  пол                           varchar(255),
  constraint pk_пол primary key (ид)
);

create table гео_объекты (
  ид                            serial not null,
  ид_род_замок_для              integer,
  ид_дома_владельца             integer,
  ид_правителя                  integer,
  constraint uq_гео_объекты_ид_род_замок_для unique (ид_род_замок_для),
  constraint pk_гео_объекты primary key (ид)
);

create table боги (
  имя                           varchar(255) not null,
  ид_религии                    integer not null,
  справ_информация              varchar(255),
  constraint pk_боги primary key (имя)
);

create table дома (
  ид                            serial not null,
  название                      varchar(255) not null,
  описание_герба                varchar(255),
  девиз                         varchar(255),
  великий_ли                    boolean default false not null,
  справ_информация              varchar(255),
  время_появления               varchar(28),
  время_исчезновения            varchar(28),
  ид_религии                    integer,
  ид_сюзерена                   integer,
  constraint uq_дома_название unique (название),
  constraint pk_дома primary key (ид)
);

create table принадл_к_дому (
  ид_дома                       integer not null,
  ид_участника                  integer not null,
  ид_роли                       integer not null,
  constraint pk_принадл_к_дому primary key (ид_дома,ид_участника,ид_роли)
);

create table организации (
  ид                            serial not null,
  название                      varchar(255) not null,
  цель                          varchar(255),
  справ_информация              varchar(255),
  время_появления               varchar(28),
  время_исчезновения            varchar(28),
  ид_вида                       integer not null,
  ид_религии                    integer,
  constraint uq_организации_название unique (название),
  constraint pk_организации primary key (ид)
);

create table участия_в_орг (
  ид_орг                        integer not null,
  ид_участника                  integer not null,
  ид_роли                       integer not null,
  constraint pk_участия_в_орг primary key (ид_орг,ид_участника,ид_роли)
);

create table виды_организаций (
  ид                            serial not null,
  наименование                  varchar(255) not null,
  constraint uq_виды_организаций_наименование unique (наименование),
  constraint pk_виды_организаций primary key (ид)
);

create table действ_лица (
  ид                            serial not null,
  имя                           varchar(255) not null,
  справ_информация              varchar(255),
  время_рождения                varchar(28),
  время_смерти                  varchar(28),
  ид_религии                    integer,
  ид_тдл                        integer,
  ид_владельца                  integer,
  ид_матери                     integer,
  ид_отца                       integer,
  ид_пола                       integer,
  constraint pk_действ_лица primary key (ид)
);

create table типы_действ_лиц (
  ид                            serial not null,
  название                      varchar(255) not null,
  справ_информация              varchar(255),
  время_появления               varchar(28),
  время_исчезновения            varchar(28),
  ид_религии                    integer,
  constraint uq_типы_действ_лиц_название unique (название),
  constraint pk_типы_действ_лиц primary key (ид)
);

create table обитания (
  ид_тдл                        integer not null,
  ид_земли                      integer not null,
  constraint pk_обитания primary key (ид_тдл,ид_земли)
);

create table религии (
  ид                            serial not null,
  название                      varchar(255) not null,
  словесн_опис_символа          varchar(255),
  справ_информация              varchar(255),
  время_появления               varchar(28),
  время_исчезновения            varchar(28),
  constraint uq_религии_название unique (название),
  constraint pk_религии primary key (ид)
);

create table роли (
  ид                            serial not null,
  наименование                  varchar(255) not null,
  constraint uq_роли_наименование unique (наименование),
  constraint pk_роли primary key (ид)
);

alter table артифакты add constraint fk_артифакты_ид_владельца foreign key (ид_владельца) references действ_лица (ид) on delete restrict on update restrict;
create index ix_артифакты_ид_владельца on артифакты (ид_владельца);

alter table гео_объекты add constraint fk_гео_объекты_ид_род_замок_для foreign key (ид_род_замок_для) references дома (ид) on delete restrict on update restrict;

alter table гео_объекты add constraint fk_гео_объекты_ид_дома_владельца foreign key (ид_дома_владельца) references дома (ид) on delete restrict on update restrict;
create index ix_гео_объекты_ид_дома_владельца on гео_объекты (ид_дома_владельца);

alter table гео_объекты add constraint fk_гео_объекты_ид_правителя foreign key (ид_правителя) references действ_лица (ид) on delete restrict on update restrict;
create index ix_гео_объекты_ид_правителя on гео_объекты (ид_правителя);

alter table боги add constraint fk_боги_ид_религии foreign key (ид_религии) references религии (ид) on delete restrict on update restrict;
create index ix_боги_ид_религии on боги (ид_религии);

alter table дома add constraint fk_дома_ид_религии foreign key (ид_религии) references религии (ид) on delete restrict on update restrict;
create index ix_дома_ид_религии on дома (ид_религии);

alter table дома add constraint fk_дома_ид_сюзерена foreign key (ид_сюзерена) references дома (ид) on delete restrict on update restrict;
create index ix_дома_ид_сюзерена on дома (ид_сюзерена);

alter table принадл_к_дому add constraint fk_принадл_к_дому_ид_дома foreign key (ид_дома) references дома (ид) on delete restrict on update restrict;
create index ix_принадл_к_дому_ид_дома on принадл_к_дому (ид_дома);

alter table принадл_к_дому add constraint fk_принадл_к_дому_ид_участника foreign key (ид_участника) references действ_лица (ид) on delete restrict on update restrict;
create index ix_принадл_к_дому_ид_участника on принадл_к_дому (ид_участника);

alter table принадл_к_дому add constraint fk_принадл_к_дому_ид_роли foreign key (ид_роли) references роли (ид) on delete restrict on update restrict;
create index ix_принадл_к_дому_ид_роли on принадл_к_дому (ид_роли);

alter table организации add constraint fk_организации_ид_вида foreign key (ид_вида) references виды_организаций (ид) on delete restrict on update restrict;
create index ix_организации_ид_вида on организации (ид_вида);

alter table организации add constraint fk_организации_ид_религии foreign key (ид_религии) references религии (ид) on delete restrict on update restrict;
create index ix_организации_ид_религии on организации (ид_религии);

alter table участия_в_орг add constraint fk_участия_в_орг_ид_орг foreign key (ид_орг) references организации (ид) on delete restrict on update restrict;
create index ix_участия_в_орг_ид_орг on участия_в_орг (ид_орг);

alter table участия_в_орг add constraint fk_участия_в_орг_ид_участника foreign key (ид_участника) references действ_лица (ид) on delete restrict on update restrict;
create index ix_участия_в_орг_ид_участника on участия_в_орг (ид_участника);

alter table участия_в_орг add constraint fk_участия_в_орг_ид_роли foreign key (ид_роли) references роли (ид) on delete restrict on update restrict;
create index ix_участия_в_орг_ид_роли on участия_в_орг (ид_роли);

alter table действ_лица add constraint fk_действ_лица_ид_религии foreign key (ид_религии) references религии (ид) on delete restrict on update restrict;
create index ix_действ_лица_ид_религии on действ_лица (ид_религии);

alter table действ_лица add constraint fk_действ_лица_ид_тдл foreign key (ид_тдл) references типы_действ_лиц (ид) on delete restrict on update restrict;
create index ix_действ_лица_ид_тдл on действ_лица (ид_тдл);

alter table действ_лица add constraint fk_действ_лица_ид_владельца foreign key (ид_владельца) references действ_лица (ид) on delete restrict on update restrict;
create index ix_действ_лица_ид_владельца on действ_лица (ид_владельца);

alter table действ_лица add constraint fk_действ_лица_ид_матери foreign key (ид_матери) references действ_лица (ид) on delete restrict on update restrict;
create index ix_действ_лица_ид_матери on действ_лица (ид_матери);

alter table действ_лица add constraint fk_действ_лица_ид_отца foreign key (ид_отца) references действ_лица (ид) on delete restrict on update restrict;
create index ix_действ_лица_ид_отца on действ_лица (ид_отца);

alter table действ_лица add constraint fk_действ_лица_ид_пола foreign key (ид_пола) references пол (ид) on delete restrict on update restrict;
create index ix_действ_лица_ид_пола on действ_лица (ид_пола);

alter table типы_действ_лиц add constraint fk_типы_действ_лиц_ид_религии foreign key (ид_религии) references религии (ид) on delete restrict on update restrict;
create index ix_типы_действ_лиц_ид_религии on типы_действ_лиц (ид_религии);

alter table обитания add constraint fk_обитания_типы_действ_лиц foreign key (ид_тдл) references типы_действ_лиц (ид) on delete restrict on update restrict;
create index ix_обитания_типы_действ_лиц on обитания (ид_тдл);

alter table обитания add constraint fk_обитания_гео_объекты foreign key (ид_земли) references гео_объекты (ид) on delete restrict on update restrict;
create index ix_обитания_гео_объекты on обитания (ид_земли);


# --- !Downs

alter table if exists артифакты drop constraint if exists fk_артифакты_ид_владельца;
drop index if exists ix_артифакты_ид_владельца;

alter table if exists гео_объекты drop constraint if exists fk_гео_объекты_ид_род_замок_для;

alter table if exists гео_объекты drop constraint if exists fk_гео_объекты_ид_дома_владельца;
drop index if exists ix_гео_объекты_ид_дома_владельца;

alter table if exists гео_объекты drop constraint if exists fk_гео_объекты_ид_правителя;
drop index if exists ix_гео_объекты_ид_правителя;

alter table if exists боги drop constraint if exists fk_боги_ид_религии;
drop index if exists ix_боги_ид_религии;

alter table if exists дома drop constraint if exists fk_дома_ид_религии;
drop index if exists ix_дома_ид_религии;

alter table if exists дома drop constraint if exists fk_дома_ид_сюзерена;
drop index if exists ix_дома_ид_сюзерена;

alter table if exists принадл_к_дому drop constraint if exists fk_принадл_к_дому_ид_дома;
drop index if exists ix_принадл_к_дому_ид_дома;

alter table if exists принадл_к_дому drop constraint if exists fk_принадл_к_дому_ид_участника;
drop index if exists ix_принадл_к_дому_ид_участника;

alter table if exists принадл_к_дому drop constraint if exists fk_принадл_к_дому_ид_роли;
drop index if exists ix_принадл_к_дому_ид_роли;

alter table if exists организации drop constraint if exists fk_организации_ид_вида;
drop index if exists ix_организации_ид_вида;

alter table if exists организации drop constraint if exists fk_организации_ид_религии;
drop index if exists ix_организации_ид_религии;

alter table if exists участия_в_орг drop constraint if exists fk_участия_в_орг_ид_орг;
drop index if exists ix_участия_в_орг_ид_орг;

alter table if exists участия_в_орг drop constraint if exists fk_участия_в_орг_ид_участника;
drop index if exists ix_участия_в_орг_ид_участника;

alter table if exists участия_в_орг drop constraint if exists fk_участия_в_орг_ид_роли;
drop index if exists ix_участия_в_орг_ид_роли;

alter table if exists действ_лица drop constraint if exists fk_действ_лица_ид_религии;
drop index if exists ix_действ_лица_ид_религии;

alter table if exists действ_лица drop constraint if exists fk_действ_лица_ид_тдл;
drop index if exists ix_действ_лица_ид_тдл;

alter table if exists действ_лица drop constraint if exists fk_действ_лица_ид_владельца;
drop index if exists ix_действ_лица_ид_владельца;

alter table if exists действ_лица drop constraint if exists fk_действ_лица_ид_матери;
drop index if exists ix_действ_лица_ид_матери;

alter table if exists действ_лица drop constraint if exists fk_действ_лица_ид_отца;
drop index if exists ix_действ_лица_ид_отца;

alter table if exists действ_лица drop constraint if exists fk_действ_лица_ид_пола;
drop index if exists ix_действ_лица_ид_пола;

alter table if exists типы_действ_лиц drop constraint if exists fk_типы_действ_лиц_ид_религии;
drop index if exists ix_типы_действ_лиц_ид_религии;

alter table if exists обитания drop constraint if exists fk_обитания_типы_действ_лиц;
drop index if exists ix_обитания_типы_действ_лиц;

alter table if exists обитания drop constraint if exists fk_обитания_гео_объекты;
drop index if exists ix_обитания_гео_объекты;

drop table if exists артифакты cascade;

drop table if exists события cascade;

drop table if exists пол cascade;

drop table if exists гео_объекты cascade;

drop table if exists боги cascade;

drop table if exists дома cascade;

drop table if exists принадл_к_дому cascade;

drop table if exists организации cascade;

drop table if exists участия_в_орг cascade;

drop table if exists виды_организаций cascade;

drop table if exists действ_лица cascade;

drop table if exists типы_действ_лиц cascade;

drop table if exists обитания cascade;

drop table if exists религии cascade;

drop table if exists роли cascade;

