package controllers;

import models.knowledgeArchive.*;
import play.mvc.*;

import views.html.*;

import java.util.List;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
public class HomeController extends Controller {

    // Find all tasks
   // List<God> gods = God.find.all();
   // List<Religion> religions = Religion.find.all();
   // List<Organisation> organisations = Organisation.find.all();
   // List<PersonKind> personKinds = PersonKind.find.all();
   // List<House> houses = House.find.all();
   // List<Person> persons = Person.find.all();

    public Result index() {
        String s = "";

//        for (Person e: persons) {
//            s += "ID: " + e.id+ "\n";
//            s += "Name: " + e.name + "\n";
//            s += "info: " + e.info+ "\n";
//            s += "Birth: " + e.birthTime + "\n";
//            s += "Death: " + e.deathTime + "\n";
//            s += "Religion: " + (e.religion == null ? "?" : e.religion.title) + "\n";
//            s += "Kind: " + (e.personKind == null ? "?" : e.personKind.title) + "\n";
//            s += "Father: " + (e.father == null ? "?" : e.father.name) + "\n";
//            s += "Mother: " + (e.mother == null ? "?" : e.mother.name) + "\n";
//            s += "Gender: " + (e.gender == null ? "?" : e.gender.title) + "\n";
//

//            if (e.memberships != null) {
//                s += "Members: ";
//                for (HouseMembership e2 : e.memberships) {
//                    s += e2.member.id + "(" + e2.role.title + ")" +  ", ";
//                }
//            }
//
//            if (e.ownedLocations != null) {
//                s += "Locations: ";
//                for (GeoLocation e2 : e.ownedLocations) {
//                    s += e2.id + ", ";
//                }
//            }


        return ok(index.render(s));
    }

}
