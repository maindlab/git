package models;

import io.ebean.Model;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class CLMSModel extends Model {
    @Id
    @Column(name = "ИД")
    public int id;
}
