package models.knowledgeArchive;

        import io.ebean.Finder;

        import javax.persistence.Column;
        import javax.persistence.Entity;
        import javax.persistence.Id;
        import javax.persistence.Table;

/**
 * Created by рома on 29.11.2017.
 */
@Entity
@Table(name="ПЕРСОНАЖ")
public class Персонаж {
    @Id
    @Column(name = "ИД")
    public Long id;

    @Column(name = "ИМЯ", nullable = false)
    public String name;

    @Column(name="РОЛЬ", nullable = false)
    public  String role;
    public static final Finder<Long, Персонаж> find = new Finder<>(Персонаж.class);
}
